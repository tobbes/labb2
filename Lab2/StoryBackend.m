//
//  StoryBackend.m
//  Lab2
//
//  Created by Tobias Ednersson on 2015-01-31.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "StoryBackend.h"
@interface StoryBackend()
@property (nonatomic) NSDictionary * Wheather;
@property (nonatomic) NSDictionary * Emotions;
@property (nonatomic) NSArray * BattleModes;
@property (nonatomic) NSDictionary * StarWars;
@property (nonatomic) NSDictionary * Normal;
@property (nonatomic) NSArray * HappyEndings;
@property (nonatomic) NSArray * BadEndings;
@property (nonatomic) NSString* Story;

- (NSString *) GetEnding: (BOOL) happy;
- (NSString *) GetPerson: (int) cat;


@end

@implementation StoryBackend



- (NSArray *) BattleModes {
    if(!_BattleModes){
        
        _BattleModes = @[@"den våldsamma envigen",@"striden",@"detta intermezzo",@"den våldsamma striden"];
    }
    return _BattleModes;
}


- (NSString *) GetBattleMode {
    
    return [self ChooseOne:self.BattleModes];
    
}



- (NSDictionary *) Emotions {
    if(!_Emotions){
        
        _Emotions = @{@"NO": @[@"upplyft",@"förnöjd",@"överlycklig",@"nöjd",@"förhoppningsfull"],
            @"YES": @[@"sur",@"irriterad",@"ledsen",@"trulig"]};
   
    }
    return _Emotions;

}




- (NSDictionary *) StarWars {
    
    if(!_StarWars) {
        
        _StarWars = @{@"namn": @[@[@"Han Solo",@"Han"],@[@"prinsessan leia",@"prinsessan"],@[@"R2D2",@"roboten"]]
                      , @"fiender": @[@[@"Kejsar palpatine",@"kejsaren"],@[@"en stormtrooper",@"stormtroopern"]],
                      @"byggnader" : @[@"jaba the huts palats",@"eco base I",@"Wampans håla"]
                      };
        
    }
    
    
    return _StarWars;
}


- (NSDictionary *) Normal {
    if(!_Normal) {
        
        _Normal = @{@"namn": @[@[@"en riddare",@"riddaren"],@[@"en magiker",@"magikern"]],
                    @"fiender": @[@[@"ett troll",@"trollet"],@[@"en demon",@"demonen"] ],
                    @"byggnader" : @[@"ett slott",@"en borg",@"en liten trädkoja"]
                    
                    
                    };
        
        
    }
    return _Normal;
}

- (NSArray *) HappyEndings {
    
    if(!_HappyEndings){
        
        _HappyEndings = @[@"De  båda segrarna levde lyckliga i alla sina dagar",
                          @"De båda hjältarna uppfann ett nytt sätt att bygga borgar på och levde rika i alla sina dagar",
                          @"Båda vinnarna hyllades  som hjältar när de kom hem, och levde tills de blev mycket gamla"];
        
    }
    
    return _HappyEndings;
}

- (NSArray *) BadEndings
{
    
    if(!_BadEndings){
        
        _BadEndings = @[@"De båda vännerna dog båda unga i lungcancer", @"De båda vinnarna spelade sedermera bort alla sina pengar och fick leva som luffare resten av sina liv", @"Det blev senare krig i de båda vännernas land och de tvingades på flykt",
                        @"Slutligen brann dock hjältarnas hus ner till grunden"];
    }
    
    return _BadEndings;
}



- (NSString *) GetBuilding: (float) starwars {
    
    int outcome = [self EitherOr:starwars];
    
    if(outcome) {
        
        return [self ChooseOne:self.StarWars[@"byggnader"]];
    }
    
    else {
        return [self ChooseOne:self.Normal[@"byggnader"]];
    }
    
    
    
}



//Getter for happy endings of the story

- (NSString *) GetEnding:(BOOL) happy {
    
    
    if(happy) {
        
        return [self ChooseOne: self.HappyEndings];
        
    }
    
    else {
        return [self ChooseOne:self.BadEndings];
    }
    
}






/* 
 Chooses wheather of the story 
 arg outcome either 1 positive, nice weather
     or 0 bad wheather
 */


- (NSString *) ChooseWheather:(int) outcome {
    
    if(outcome){
        return [self ChooseOne:self.Wheather[@"bra"]];
    }
    else {
        return [self ChooseOne:self.Wheather[@"daligt"]];
    }
}




- (int) EitherOr: (float) prob {
    
    //long hej = arc4random();
    //float tjo = (arc4random()  / (float)LONG_MAX);
    
    float tjo = arc4random() % 100;
    if(tjo <= prob){
        return 1;
    }
    
    else {
        return 0;
    }
    }



/* Gets a random element from the array provided */

- (NSString *) ChooseOne:(NSArray *) options {
    return options[arc4random()%options.count];
}

/* Gets a random array from the provided array of arrays */

- (NSArray *) ChooseOneArray:(NSArray *) options {
    return options[arc4random()%options.count];
}


 
 /*
 Randomizes a character for the story 
 argument cat is either 1 for starwars character 
 or 0 for a "normal" character
 */
- (NSArray *) GetPerson:(int) cat
{
    NSArray * attSkicka = nil;
    if(cat == 1){
    
        attSkicka = self.StarWars[@"namn"];
        
    }
    
    else {
        attSkicka = self.Normal[@"namn"];
    }
    
    
    return [self ChooseOneArray:attSkicka];
    
    
}



/* Gets a random antagonist for the story */
/* cat is either 1 for starwars enemy or 0 for "normal" enemy */
- (NSArray *) GetEnemy: (int) cat {
 
    
    NSArray * attSkicka = nil;
    if(cat == 1){
        
        attSkicka = self.StarWars[@"fiender"];
        
    }
    
    else {
        attSkicka = self.Normal[@"fiender"];
    }
    
    
    return [self ChooseOneArray:attSkicka];
    
}







-(NSDictionary *) Wheather {
    
    if(! _Wheather) {
        _Wheather =
        @{@"bra" : @[@"soligt",@"varmt",@"strålande"],
          @"daligt" : @[@"uselt",@"regnigt",@"grått",@"deprimeerande"]};
        
    }
    
    return _Wheather;
}













/* Main method called from controller, given amount happyending true or false, an amount of starwars
 and an amount of gloom, generates a story, which takes these parameters into consideration
 */

- (NSString *) GenerateStoryWith:(BOOL) happyEnd and: (float) starwars and: (float) gloom {
    
    
    NSString * toReturn;
    NSString * ending = [self GetEnding:happyEnd];
    
    int outcome = [self EitherOr:starwars];
    
    NSArray * person = [self GetPerson:outcome];
    
    
    NSString * weather;
    
    outcome = [self EitherOr:gloom];
    
    weather = [self GetWeather:(outcome == 1)];
 
    NSArray * person2 = [self GetPerson:[self EitherOr:starwars]];
    
    NSArray * enemy = [ self GetEnemy:[self EitherOr:starwars]];
    NSString * emotion1 = [self GetEmotion:gloom];
    NSString * emotion2 = [self GetEmotion:gloom];
    
    
    NSString * battleMode = [self GetBattleMode];
    
    toReturn = [NSString stringWithFormat:self.Story,weather,person[0], person[1]
                , enemy[0],enemy[1],person[1],emotion1,person2[0],person2[1],person[0],enemy[1],battleMode,person[0],person2[0],enemy[0],
                person[0],person2[0],enemy[0],emotion2,[self GetBuilding:starwars],[self GetBuilding:starwars]];
    
    toReturn = [toReturn stringByAppendingString:ending];
    
    return toReturn;
    
}



/* Main strukture of the story */
- (NSString * ) Story {
    if(!_Story){
        _Story = @"Det var dag, vädret var %@ på en liten väg kom %@ till sin stora förskräckelse träffade %@ "
        " i buskarna på %@, %@ gick genast glatt till anfall under glada tillrop"
        " Nu blev %@ synnerligen %@. Plötsligt dök dock hjälp upp från oväntat håll: %@ gjorde entré och %@ kände det givetvis som sin moraliska förpliktelse "
        "att hjälpa %@ i dennes värv att besegra %@ \n\n "
        "Vapnen svingades, kombattanterna svettades, hur skulle %@ sluta? \n"
        "Detta är ju fruktansvärt spännande! Till sist blev dock våra hjältar: %@ och %@ övermäktiga för %@\n "
        "Han kastade sitt vapen och flydde hals över huvud och %@ och %@ kunde fira sin seger\n"
        "%@ var dock märkbart %@, på vägen sprang han förbi %@ och %@ ";
    }
    return _Story;
}






/* Gets a ranom wheater */
- (NSString *) GetWeather: (BOOL) bad {
    
    
    if(bad){
        
        return [self ChooseOne: self.Wheather[@"daligt"]];
        
    }
   
    else {
        
        return [self ChooseOne:self.Wheather[@"bra"]];
    }
    
}


/* Given the amount of gloom returns a random emotion 
    probability for sad/bad feelings increases with amount of gloom
 
 */
- (NSString *) GetEmotion:(float) gloom {
    
    
    int outcome = [self EitherOr:gloom ];
    
    
    
    
    if(outcome ) {
        return [self ChooseOne:self.Emotions[@"YES"]];
    }
    else {
        return [self ChooseOne:self.Emotions[@"NO"]];
    }
    
    
}





@end












