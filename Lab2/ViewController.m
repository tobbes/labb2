//
//  ViewController.m
//  Lab2
//
//  Created by Tobias Ednersson on 2015-01-28.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import "ViewController.h"
#import "StoryBackend.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UISwitch *EndsWell;
@property (weak, nonatomic) IBOutlet UITextView *Story;
@property (strong, nonatomic) StoryBackend * backend;
@property (weak, nonatomic) IBOutlet UISlider *StarwarsSlider;

@property (weak, nonatomic) IBOutlet UISlider *GloomSlider;


@end

@implementation ViewController

- (StoryBackend *) backend {
    
    if(!_backend){
        _backend = [[StoryBackend alloc] init];
        
    }
    return _backend;
}





- (IBAction)GenerateStory:(id)sender {
    
    [self.Story setText: @""];
    //NSString * story = [NSString stringWithString:self.backend.Story];
    self.Story.text = nil;
    self.Story.text = nil;
    NSString * result = [self.backend GenerateStoryWith:self.EndsWell.isOn and:self.StarwarsSlider.value and: self.GloomSlider.value];
    
    
    [self.Story setText: result];
    
    
    
    
   
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
