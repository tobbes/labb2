//
//  StoryBackend.h
//  Lab2
//
//  Created by Tobias Ednersson on 2015-01-31.
//  Copyright (c) 2015 Tobias Ednersson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoryBackend : NSObject
//


- (NSString *) GenerateStoryWith:(BOOL) happyEnd and: (float) starwars and: (float) gloom;

@end
